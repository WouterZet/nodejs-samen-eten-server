const express = require('express');
const app = express();
const port = process.env.PORT || 3000;

app.get("/api/info", (req, res) => {
  let result = {
    Message: "Wouter Zegers 2172665"
  }
  res.status(401).json(result)
});

app.listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}/`);
});